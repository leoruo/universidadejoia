﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UniversidadeJoia.Models;

namespace UniversidadeJoia.Controllers
{
    public class InstituicaoController : Controller
    {
        private static IList<Instituicao> instituicoes =
            new List<Instituicao>()
            {
                new Instituicao()
                {
                    InstituicaoId = 1,
                    Nome = "UniParaná",
                    Endereco = "Paraná"
                },
                new Instituicao()
                {
                    InstituicaoId = 2,
                    Nome = "UniSanta",
                    Endereco = "Santa Catarina"
                },
                new Instituicao()
                {
                    InstituicaoId = 3,
                    Nome = "UniSãoPaulo",
                    Endereco = "São Paulo"
                },
                new Instituicao()
                {
                    InstituicaoId = 4,
                    Nome = "UniSulGrandense",
                    Endereco = "Rio Grande do Sul"
                },
                new Instituicao()
                {
                    InstituicaoId = 5,
                    Nome = "UniCarioca",
                    Endereco = "Rio de Janeiro"
                },
            };

        public IActionResult Index()
        {
            return View(instituicoes);
        }

        // GET : Create
        public ActionResult Create()
        {
            return View();
        }

        // POST : Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Instituicao instituicao)
        {
            instituicao.InstituicaoId =
                instituicoes.Select(i => i.InstituicaoId).Max() + 1;
            instituicoes.Add(instituicao);
            return RedirectToAction("Index");
        }
    }
}